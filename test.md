|id|tester|test data|result|expect result|fix|
|:---:|---|---|---|---|---|
|1|me|enemy_def>player_atk|負值得攻擊輸出|輸出始終需>=0|確保玩家攻擊有效，否則就逃跑|
|2|me|遊戲結束|玩家死亡依然獲得經驗值|死亡後不該取得怪物經驗|重新設計`if`邏輯|
|3|me|未讀取前儲存|跳出錯誤並且覆蓋原本角色|提示錯誤並跳出迴圈|exception handling|
