from .classes import Warrior, Sorcerer, Knight, Archer


def make_player(inh_class_name):
    """
    Dynamic inheritance
    """
    def hp_ratio(class_name):
        if class_name == "Warrior":
            ratio = 400
        elif class_name == "Sorcerer":
            ratio = 100
        elif class_name == "Knight":
            ratio = 300
        elif class_name == "Archer":
            ratio = 200
        else:
            ratio = 1
        return ratio

    def check(inh_class):
        if inh_class == "Warrior":
            return Warrior
        elif inh_class == "Knight":
            return Knight
        elif inh_class == "Sorcerer":
            return Sorcerer
        else:
            return Archer

    class player(check(inh_class_name)):
        def __init__(self):
            """
            Set race for human-kinds at 1-to-1 manner
            Given extra attributes based on class
            """
            super().__init__()
            if super().career() == "Warrior":
                self.set_race("Dwarf")
            elif super().career() == "Sorcerer":
                self.set_race("Mage")
            elif super().career() == "Knight":
                self.set_race("Human")
            elif super().career() == "Archer":
                self.set_race("Elf")

            """
            extra properties for a player
            hp = hp_ratio
            atk = power + agile
            def = agile + intel
            """
            self.__atk = int(self.power(1) + self.agile(1))
            self.__defense = int(self.agile(1) + self.intel(1))

        """
        Expose properties
        """

        def hp(self):
            if self.lv() != 1:
                hp = int(hp_ratio(self.career()) * (1.01 ** self.lv()))
            else:
                hp = int(hp_ratio(super().career()))
            return hp

        def atk(self):
            return int(self.power(self.lv()) + self.agile(self.lv()))

        def defense(self):
            return int(self.agile(self.lv()) + self.intel(self.lv()))

        def gain_exp(self, exp):
            self.set_exp(self.exp() + exp)

        """
        Leverage logic for properties
        """

        def required_exp(self, lv):
            """
            We use fixed exp range for levels
            """
            return lv * 100

        def check_upgrade(self):
            while self.exp() >= self.required_exp(self.lv()):
                self.set_exp(self.exp() - self.required_exp(self.lv()))
                self.set_lv(self.lv() + 1)
    return player
