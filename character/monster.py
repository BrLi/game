from .classes import Warrior, Sorcerer, Knight, Archer


def make_monster(inh_class_name):
    """
    Dynamic inheritance
    """
    def hp_ratio(class_name):
        if class_name == "Warrior":
            ratio = 400
        elif class_name == "Sorcerer":
            ratio = 100
        elif class_name == "Knight":
            ratio = 300
        elif class_name == "Archer":
            ratio = 200
        else:
            ratio = 1
        return ratio

    def check(inh_class):
        if inh_class == "Warrior":
            return Warrior
        elif inh_class == "Knight":
            return Knight
        elif inh_class == "Sorcerer":
            return Sorcerer
        else:
            return Archer

    class monster(check(inh_class_name)):
        def __init__(self):
            """
            Set race for human-kinds at 1-to-1 manner
            Given extra attributes based on class
            """
            super().__init__()
            if super().career() == "Warrior":
                self.set_race("Orge")
            elif super().career() == "Sorcerer":
                self.set_race("Shaman")
            elif super().career() == "Knight":
                self.set_race("Goblin")
            elif super().career() == "Archer":
                self.set_race("Hunter")
            """
            extra properties for a player
            base_hp = hp_ratio
            base_atk = power + agile
            base_def = agile + intel
            exp to be passed to player = lv*10
            """
            self.__base_hp = hp_ratio(self.career())
            self.__base_atk = (self.power(self.lv()) + self.agile(self.lv()))
            self.__base_def = (self.agile(self.lv()) + self.intel(self.lv()))

        """
        Expose properties
        """

        def hp(self):
            return self.__base_hp

        def atk(self):
            return self.__base_atk

        def defense(self):
            return self.__base_def

    return monster
