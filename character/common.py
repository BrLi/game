class Common():
    """
    Define common specs of player
    """

    def __init__(self):
        self.__race = "Human"
        self.__career = "Norm"
        self.__exp = 0
        self.__lv = 1
        self.__base_power = 0
        self.__base_agile = 0
        self.__base_intel = 0

    """
    Basic return of private values
    """

    def race(self):
        return self.__race

    def career(self):
        return self.__career

    def exp(self):
        return self.__exp

    def lv(self):
        return self.__lv

    def power(self, lv):
        if lv > 1:
            return int(self.__base_power * (1.1 ** lv))
        elif lv == 1:
            return self.__base_power

    def agile(self, lv):
        if lv > 1:
            return int(self.__base_agile * (1.1 ** lv))
        elif lv == 1:
            return self.__base_agile

    def intel(self, lv):
        if lv > 1:
            return int(self.__base_intel * (1.1 ** lv))
        elif lv == 1:
            return self.__base_intel

    """
    Define valid operations on private values
    """

    def set_race(self, race):
        self.__race = race

    def set_class(self, career):
        self.__career = career

    def set_exp(self, exp):
        self.__exp = exp

    def set_lv(self, lv):
        self.__lv = lv

    def set_power(self, power):
        self.__base_power = power

    def set_agile(self, agile):
        self.__base_agile = agile

    def set_intel(self, intel):
        self.__base_intel = intel
