from .common import Common
from random import randint as ri

"""
Set name, power, agile, intel for different classes
"""


class Archer(Common):
    """
    Inherit from Common class
    """

    def __init__(self):
        super().__init__()
        super().set_class("Archer")
        super().set_power(ri(1, 333))
        super().set_agile(ri(667, 999))
        super().set_intel(ri(667, 999))


class Knight(Common):
    """
    Inherit from Common class
    """

    def __init__(self):
        super().__init__()
        super().set_class("Knight")
        super().set_power(ri(334, 666))
        super().set_agile(ri(334, 666))
        super().set_intel(ri(334, 666))


class Sorcerer(Common):
    """
    Inherit from Common class
    """

    def __init__(self):
        super().__init__()
        super().set_class("Sorcerer")
        super().set_power(ri(1, 333))
        super().set_agile(ri(1, 333))
        super().set_intel(ri(667, 999))


class Warrior(Common):
    """
    Inherit from Common class
    """

    def __init__(self):
        super().__init__()
        super().set_class("Warrior")
        super().set_power(ri(667, 999))
        super().set_agile(ri(1, 333))
        super().set_intel(ri(1, 333))
