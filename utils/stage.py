from character import player, monster
from random import randint

# def advantage(p1, p2):
#     """
#     classes are taking advantage over one another.
#     """


def monster_class(num):
    if num % 4 == 0:
        return "Archer"
    if num % 4 == 1:
        return "Knight"
    if num % 4 == 2:
        return "Sorcerer"
    if num % 4 == 3:
        return "Warrior"


"""
spawn 1 monsters
check to fetch 1 player
player attack first
pass stage confirmation
define player status (live/dead) based on HP not 0
"""


def spawn_enemy(member):
    m = monster.make_monster(monster_class(randint(1, 100)))()
    player_lv = member.lv()
    m_lv = randint(player_lv, player_lv+2)
    m.set_lv(m_lv)
    m.set_exp(m_lv * 100)
    return m


def conflicts(member, enemy):
    """
    player attack enemy1 until enemy hp = 0
    then go to enemy2, etc. etc.
    """

    player_hp = member.hp()
    player_atk = member.atk()
    player_def = member.defense()

    enemy_hp = enemy.hp()
    enemy_atk = enemy.atk()
    enemy_def = enemy.defense()

    if player_atk > enemy_def:
        while (player_hp >= 0) and (enemy_hp >= 0):
            print(player_atk - enemy_def, "damage to", enemy.race())
            enemy_hp -= player_atk - enemy_def
            print(enemy_hp if enemy_hp > 0 else 0, "life points remained.")
            if enemy_hp > 0:
                print(enemy_atk - player_def,
                      "damage to player: ", member.race())
                player_hp -= enemy_atk - player_def
                print(player_hp if player_hp >
                      0 else 0, "life points remained.")
    else:
        print("You run away!")

    if enemy_hp <= 0:
        member.gain_exp(enemy.exp())
        member.check_upgrade()
    else:
        pass


def end_game(member, enemy):
    """
    either player HP <= 0
    or stage enemies HP = 0
    """
    pass


def start_game(member):
    """
    check player status
    check enemy status
    """
    monster = spawn_enemy(member)
    print("monster lv is", monster.lv())
    conflicts(member, monster)
