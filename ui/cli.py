from character import player, monster
from utils import stage


def spawn_chara(member_type):
    """
    function to spawn character from player or monster
    """
    character = str(input("選擇一個職業起手：Archer, Knight, Sorcerer, Warrior: "))
    if character not in ["Archer", "Knight", "Sorcerer", "Warrior"]:
        print("STOP!!!!")
    else:
        newmember = player.make_player(character)()
    return newmember


def display_status(member):
    print("Lv", member.lv())
    print("Exp", member.exp())
    print("Basic Properties")
    print("Race", member.race())
    print("Type", member.career())
    print("Power, Agile, Intellegence", member.power(member.lv()),
          member.agile(member.lv()), member.intel(member.lv()))
    print("HP, Atk, Def", member.hp(), member.atk(), member.defense())
    if type(member).__name__ == "player":
        print("Required Exp for next Lv", member.required_exp(member.lv()))


while True:
    first_input = input(
        "輸入數字選擇你想做什麼？\n1. 創新角色\n2. 從檔案讀取角色\n3. 紀錄角色到檔案\n4. 開始遊戲\n（其他任意鍵將結束程式）： ")
    if first_input == "1":
        try:
            new_player = spawn_chara("player")
            display_status(new_player)
        except UnboundLocalError:
            print("居然反悔嗎？")
    elif first_input == "2":
        try:
            with open('player.txt', 'r', encoding='utf-8') as player_file:
                array = player_file.read().splitlines()
            player_file.close()
            new_player = player.make_player(array[3])()
            new_player.set_lv(int(array[0]))
            new_player.set_exp(int(array[1]))
            new_player.set_race(array[2])
            new_player.set_class(array[3])
            new_player.set_power(int(array[4]))
            new_player.set_agile(int(array[5]))
            new_player.set_intel(int(array[6]))
            display_status(new_player)
        except FileNotFoundError:
            print("There is nothing in your pocket yet.")

    elif first_input == "3":
        try:
            new_player.lv()
            with open('player.txt', 'w+', encoding='utf-8') as player_file:
                player_file.write(str(new_player.lv())+"\n")
                player_file.write(str(new_player.exp())+"\n")
                player_file.write(new_player.race()+"\n")
                player_file.write(new_player.career()+"\n")
                player_file.write(str(new_player.power(new_player.lv()))+"\n")
                player_file.write(str(new_player.agile(new_player.lv()))+"\n")
                player_file.write(
                    str(new_player.intel(new_player.lv())) + "\n")
        except:
            print("目前沒有資料")

    elif first_input == "4":
        try:
            stage.start_game(new_player)
            display_status(new_player)
        except NameError:
            print("目前還沒有角色")

    else:
        print("Bye!")
        break
